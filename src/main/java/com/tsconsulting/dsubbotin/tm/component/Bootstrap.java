package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.*;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.project.*;
import com.tsconsulting.dsubbotin.tm.command.system.*;
import com.tsconsulting.dsubbotin.tm.command.task.*;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownArgumentException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;
import com.tsconsulting.dsubbotin.tm.repository.CommandRepository;
import com.tsconsulting.dsubbotin.tm.repository.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.TaskRepository;
import com.tsconsulting.dsubbotin.tm.service.*;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ArgumentsDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());

        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateStatusByIdCommand());
        registry(new ProjectUpdateStatusByIndexCommand());
        registry(new ProjectUpdateStatusByNameCommand());

        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateStatusByIdCommand());
        registry(new TaskUpdateStatusByIndexCommand());
        registry(new TaskUpdateStatusByNameCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskAllByProjectIdCommand());
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    public void run(final String[] args) {
        try {
            TerminalUtil.printMessage("** WELCOME TO TASK MANAGER **");
            initData();
            parseArgs(args);
            process();
        } catch (Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void parseArgs(final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (command == null) throw new UnknownArgumentException();
        command.execute();
        command = commandService.getCommandByName("exit");
        command.execute();
    }

    private void process() throws AbstractException {
        logService.debug("Test environment!");
        String command = "";
        while (!isExitCommand(command)) {
            command = TerminalUtil.nextLine();
            logService.command(command);
            runCommand(command);
            logService.info("Commands '" + command + "' executed!");
        }
    }

    private void runCommand(final String command) {
        try {
            if (command == null || command.isEmpty()) throw new UnknownCommandException();
            final AbstractCommand abstractCommand = commandService.getCommandByName(command);
            if (abstractCommand == null) throw new UnknownCommandException();
            abstractCommand.execute();
        } catch (Exception e) {
            logService.error(e);
        }
    }

    private void registry(final AbstractCommand command) {
        try {
            if (command == null) throw new UnknownCommandException();
            command.setServiceLocator(this);
            commandService.add(command);
        } catch (Exception e) {
            logService.error(e);
        }
    }

    private boolean isExitCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand;
        abstractCommand = commandService.getCommandByName("exit");
        if (abstractCommand == null) return false;
        return abstractCommand.name().equals(command);
    }

    private void initData() {
        try {
            projectService.create("D_Project_1", "1");
            projectService.create("C_Project_2", "2");
            projectService.create("A_Project_3", "3");
            projectService.create("B_Project_4", "4");
            projectService.create("E_Project_5", "5");

            projectService.startByIndex(1);
            projectService.finishByIndex(2);

            taskService.create("B_Task_1", "1");
            taskService.create("A_Task_2", "2");
            taskService.create("C_Task_3", "3");
            taskService.create("E_Task_4", "4");
            taskService.create("D_Task_5", "5");

            taskService.finishByIndex(3);
            taskService.startByIndex(4);
        } catch (AbstractException e) {
            logService.error(e);
        }
    }

}
