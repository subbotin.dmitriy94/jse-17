package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) throws AbstractException {
        checkName(name);
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name,
                       final String description) throws AbstractException {
        checkName(name);
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return Collections.emptyList();
        return projectRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(final String id) throws AbstractException {
        checkId(id);
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final int index) throws AbstractException {
        checkId(index);
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(final String name) throws AbstractException {
        checkName(name);
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateById(final String id,
                              final String name,
                              final String description) throws AbstractException {
        checkId(id);
        checkName(name);
        return projectRepository.updateBuyId(id, name, description);
    }

    @Override
    public Project updateByIndex(final int index,
                                 final String name,
                                 final String description) throws AbstractException {
        if (index < 0) throw new EmptyIndexException();
        checkName(name);
        return projectRepository.updateBuyIndex(index, name, description);
    }

    @Override
    public Project startById(final String id) throws AbstractException {
        checkId(id);
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final int index) throws AbstractException {
        checkId(index);
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(final String name) throws AbstractException {
        checkName(name);
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) throws AbstractException {
        checkId(id);
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(final int index) throws AbstractException {
        checkId(index);
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(final String name) throws AbstractException {
        checkName(name);
        return projectRepository.finishByName(name);
    }

    @Override
    public Project updateStatusById(final String id,
                                    final Status status) throws AbstractException {
        checkId(id);
        return projectRepository.updateStatusById(id, status);
    }

    @Override
    public Project updateStatusByIndex(final int index,
                                       final Status status) throws AbstractException {
        checkId(index);
        return projectRepository.updateStatusByIndex(index, status);
    }

    @Override
    public Project updateStatusByName(final String name,
                                      final Status status) throws AbstractException {
        checkName(name);
        return projectRepository.updateStatusByName(name, status);
    }

    private void checkId(String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
    }

    private void checkId(int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

    private void checkName(String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
    }

}
