package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ICommandService;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) throws AbstractException {
        if (arg == null || arg.isEmpty()) throw new EmptyNameException();
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractSystemCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public void add(final AbstractCommand command) throws UnknownCommandException {
        if (command == null) throw new UnknownCommandException();
        commandRepository.add(command);
    }

}
