package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) throws AbstractException {
        checkName(name);
        Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name,
                       final String description) throws AbstractException {
        checkName(name);
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return Collections.emptyList();
        return taskRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) throws AbstractException {
        checkId(id);
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) throws AbstractException {
        checkIndex(index);
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) throws AbstractException {
        checkName(name);
        return taskRepository.findByName(name);
    }

    @Override
    public Task removeById(final String id) throws AbstractException {
        checkId(id);
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) throws AbstractException {
        if (index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) throws AbstractException {
        checkName(name);
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateById(final String id,
                           final String name,
                           final String description) throws AbstractException {
        checkId(id);
        checkName(name);
        return taskRepository.updateBuyId(id, name, description);
    }

    @Override
    public Task updateByIndex(final int index,
                              final String name,
                              final String description) throws AbstractException {
        checkIndex(index);
        checkName(name);
        return taskRepository.updateBuyIndex(index, name, description);
    }

    @Override
    public Task startById(final String id) throws AbstractException {
        checkId(id);
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(final int index) throws AbstractException {
        checkIndex(index);
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(final String name) throws AbstractException {
        checkName(name);
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(final String id) throws AbstractException {
        checkId(id);
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(final int index) throws AbstractException {
        checkIndex(index);
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(final String name) throws AbstractException {
        checkName(name);
        return taskRepository.finishByName(name);
    }

    @Override
    public Task updateStatusById(final String id,
                                 final Status status) throws AbstractException {
        checkId(id);
        return taskRepository.updateStatusById(id, status);
    }

    @Override
    public Task updateStatusByIndex(final int index,
                                    final Status status) throws AbstractException {
        checkIndex(index);
        return taskRepository.updateStatusByIndex(index, status);
    }

    @Override
    public Task updateStatusByName(final String name,
                                   final Status status) throws AbstractException {
        checkName(name);
        return taskRepository.updateStatusByName(name, status);
    }

    private void checkName(String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
    }

    private void checkId(String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
    }

    private void checkIndex(Integer index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}