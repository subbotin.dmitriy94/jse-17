package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        TerminalUtil.printMessage("Id: " + project.getId() + "\n" +
                "Name: " + project.getName() + "\n" +
                "Description: " + project.getDescription() + "\n" +
                "Status: " + project.getStatus().getDisplayName() + "\n" +
                "Create date: " + project.getCreateDate() + "\n" +
                "Start date: " + project.getStartDate()
        );
    }

    protected Project create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return new Project(name, description);
    }

}
