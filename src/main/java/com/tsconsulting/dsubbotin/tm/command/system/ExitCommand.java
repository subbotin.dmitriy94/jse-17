package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public class ExitCommand extends AbstractSystemCommand {

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Exit application.";
    }

    @Override
    public void execute() throws AbstractException {
        System.exit(0);
    }

}
