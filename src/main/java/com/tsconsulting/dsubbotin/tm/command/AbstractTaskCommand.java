package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        TerminalUtil.printMessage("Id: " + task.getId() + "\n" +
                "Name: " + task.getName() + "\n" +
                "Description: " + task.getDescription() + "\n" +
                "Status: " + task.getStatus().getDisplayName() + "\n" +
                "Create date: " + task.getCreateDate() + "\n" +
                "Start date: " + task.getStartDate()
        );
    }

    protected Task create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return new Task(name, description);
    }

}
