package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class ArgumentsDisplayCommand extends AbstractSystemCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Display list arguments.";
    }

    @Override
    public void execute() throws AbstractException {
        for (final AbstractSystemCommand command : serviceLocator.getCommandService().getArguments()) {
            final String argument = command.arg();
            if (argument != null && !argument.isEmpty()) TerminalUtil.printMessage(argument);
        }
    }

}
