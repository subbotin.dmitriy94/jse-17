package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {
    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Override
    public String description() {
        return "Display project by index.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findByIndex(index);
        showProject(project);
    }

}
