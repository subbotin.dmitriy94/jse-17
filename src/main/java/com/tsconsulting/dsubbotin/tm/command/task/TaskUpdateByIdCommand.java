package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateById(id, name, description);
        TerminalUtil.printMessage("[Task updated]");
    }

}
