package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.List;

public class TaskAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list-by-project-id";
    }

    @Override
    public String description() {
        return "Display task list by project id.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(projectId);
        List<Task> tasks = serviceLocator.getProjectTaskService().findAllTasksByProjectId(projectId);
        int index = 1;
        for (Task task : tasks) TerminalUtil.printMessage(index++ + ". " + task.toString());
    }

}
