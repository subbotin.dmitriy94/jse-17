package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Display project list.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        try {
            Sort sortType = Sort.valueOf(sort);
            TerminalUtil.printMessage(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(sortType.getComparator());
        } catch (IllegalArgumentException e) {
            projects = serviceLocator.getProjectService().findAll();
        }
        int index = 1;
        for (Project project : projects) TerminalUtil.printMessage(index++ + ". " + project);
    }

}
