package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class AboutDisplayCommand extends AbstractSystemCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Developer: Dmitriy Subbotin");
        TerminalUtil.printMessage("E-Mail: dsubbotin@tsconsulting.com");
    }

}
