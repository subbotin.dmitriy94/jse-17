package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class DisplayCommand extends AbstractSystemCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String description() {
        return "Display list commands.";
    }

    @Override
    public void execute() throws AbstractException {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            TerminalUtil.printMessage(command.name());
        }
    }

}
