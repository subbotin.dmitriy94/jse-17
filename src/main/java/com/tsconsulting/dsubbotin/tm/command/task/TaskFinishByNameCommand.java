package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish task by name.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().finishByName(name);
    }

}
