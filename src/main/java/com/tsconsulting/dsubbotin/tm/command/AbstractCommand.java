package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws AbstractException;

    @Override
    public String toString() {
        StringBuilder sbCommandToString = new StringBuilder();
        final String name = name();
        final String description = description();
        if (name != null && !name.isEmpty()) sbCommandToString.append(name);
        if (description != null && !description.isEmpty()) sbCommandToString.append(" - ").append(description);
        return sbCommandToString.toString();
    }

}
