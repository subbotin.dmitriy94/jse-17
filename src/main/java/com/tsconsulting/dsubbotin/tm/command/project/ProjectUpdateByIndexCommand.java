package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getProjectService().findByIndex(index);
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateByIndex(index, name, description);
        TerminalUtil.printMessage("[Updated project]");
    }

}
