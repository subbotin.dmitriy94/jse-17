package com.tsconsulting.dsubbotin.tm.enumerated;

import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByCreated;
import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByName;
import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByStartDate;
import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.INSTANCE),
    NAME("Sort by name", ComparatorByName.INSTANCE),
    START_DATE("Sort by date start", ComparatorByStartDate.INSTANCE),
    STATUS("Sort by status", ComparatorByStatus.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
